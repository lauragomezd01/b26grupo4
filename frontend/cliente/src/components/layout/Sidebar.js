import React from "react"; //imr
import Listado from "../projects/Listado";
import NuevoProyecto from "../projects/NuevoProyecto";

//sfc
const Sidebar = () => {
    return (
        <aside>
            <h1>
                MERN <span>Task</span>
            </h1>
            <NuevoProyecto />
            <div className="proyectos">
                <h2>Tus Proyectos</h2>
                <Listado />
            </div>
        </aside>
    );
}

export default Sidebar;