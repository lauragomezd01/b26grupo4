import React ,{Fragment, useState} from 'react';

const NuevoProyecto = (e) => {
  
    const [proyecto, guardarProyecto] = useState({
        nombre:''        
    })
   
    const {nombre} = proyecto;

    const onChangeProyecto= e =>{
        guardarProyecto({
            ...proyecto,
            [e.target.name]:e.target.value
        })
    }


    const onSubmitProyecto= e =>{
        e.preventDefault();

        //Validación del proyecto

        //Usar o agregar al state

        //reiniciar el form

    }

    return(
       <Fragment>
           <button type="button" className="btn btn-block btn-primario" onSubmit={onSubmitProyecto}>
               Nuevo Proyecto
           </button>
           <form className="formulario-nuevo-proyecto">
                <input 
                type="text"
                className="input-text"
                placeholder="Nombre del proyecto"
                name="nombre"
                onChange={onChangeProyecto}
                value={nombre}
                />
                <input 
                type="submit"
                className="btn btn-primario btn-block"
                value="Agregar Proyecto"
                />
           </form>
       </Fragment>
   );  
};

export default NuevoProyecto;
