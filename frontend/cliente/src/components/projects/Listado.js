import React from 'react'; //imr
import Project from './Project';

//sfc
const Listado = () => {
    
    const proyectos=[
        {nombre:"Tienda Virtual"},
        {nombre: "Intranet"},
        {nombre: "Diseño de Sitio Web"}
    ];
    
    return ( 
    <ul className="listado-proyectos">
        {proyectos.map((project)=>(
            <Project project={project} />
        ))}
    </ul> 
    );
}
 
export default Listado;
