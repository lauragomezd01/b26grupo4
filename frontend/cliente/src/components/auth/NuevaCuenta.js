import {React, useState} from 'react';
import { Link } from 'react-router-dom';



const NuevaCuenta = () => {

    //State del usuario
    const [usuario,guardarUsuario] = useState(
        {
        nombre:'',
        email:'',
        password:'',
        cpassword:''
        }
    )

    const {email, nombre, password, cpassword} = usuario;

    const onChange = e =>{
        //console.log(e);
        guardarUsuario({
            ...usuario,
            //Reemplaza lo que llega en el campo del nombre del usuario
            [e.target.name]:e.target.value
        })
    }

    //Cuando el usuario quiera iniciar sesión
    const onSubmit = e =>{
        e.preventDefault();

        //Validar que no haya campos vacíos

        //Validar que el passworl tenga mínimo 6 caracteres

        //Los dos password son iguales

        //Envío de los datos
    }

    return ( 
    <div className="form-usuario">
        <div className="contenedor-form sombra-dark">
            <h1>Registro</h1>
            <form onSubmit={onSubmit}>
                <div className="campo-form">
                    <label htmlFor="nombre">Nombre</label>
                    <input  
                    type="text"
                    id="nombre"
                    name="nombre"
                    placeholder="Tu nombre"
                    value = {nombre}
                    onChange = {onChange}
                    />
                </div>
                <div className="campo-form">
                    <label htmlFor="email">Email</label>
                    <input  
                    type="email"
                    id="email"
                    name="email"
                    placeholder="Tu email"
                    value = {email}
                    onChange = {onChange}
                    />
                </div>
                <div className="campo-form">
                    <label htmlFor="password">Password</label>
                    <input  
                    type="password"
                    id="password"
                    name="password"
                    placeholder="Tu password"
                    value={password}
                    onChange = {onChange}
                    />
                </div>
                <div className="campo-form">
                    <label htmlFor="cpassword">Confirmar Password</label>
                    <input  
                    type="password"
                    id="cpassword"
                    name="cpassword"
                    placeholder="Confirma tu password"
                    value={cpassword}
                    onChange = {onChange}
                    />
                </div>
                <div classname="campo-form">
                    <input type="submit" className="btn btn-primario btn-block"
                    value = "Iniciar Sesión"

                    />
                </div>
            </form>
            <Link to={"/"} className="enlace-cuenta">
                Iniciar Sesión
            </Link>
        </div>
        
    </div>
    );
}
 
export default NuevaCuenta;