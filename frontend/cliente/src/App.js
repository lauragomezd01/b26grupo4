import React from 'react';

import Projects from './components/projects/Projects';
import NuevaCuenta from './components/auth/NuevaCuenta';
import Login from './components/auth/Login';

import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';

function App() {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<Login />}/>
        <Route path="/nueva-cuenta" element={<NuevaCuenta />}/>
        <Route path="/proyectos" element={<Projects />}/>
      </Routes>
    </Router>
  );
}

export default App;
