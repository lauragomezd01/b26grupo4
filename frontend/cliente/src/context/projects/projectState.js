import React, {useReducer} from 'react';
import projectContext from './projectContext';
import projectReducer from './projectReducer';

const projectState = props=>{
    const initialState={
        nuevoProyecto:false
    }

    const [state, dispatch]= useReducer(projectReducer, initialState);

}